import pandas as pd
import numpy as np


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''

    titles = ["Mr.", "Mrs.", "Miss."]
    
    def extract_title(name):
        for t in titles:
            if t in name:
                return t
        return None

    df['Title'] = df['Name'].apply(extract_title)
    
    # Initialize results list
    results = []

    for title in titles:
        group = df[df['Title'] == title]
        mis_ages_count = group['Age'].isnull().sum()
        median_age = round(group['Age'].median())
        results.append((title, mis_ages_count, median_age))
        
        df.loc[(df['Age'].isnull()) & (df['Title'] == title), 'Age'] = median_age
    
    return results
